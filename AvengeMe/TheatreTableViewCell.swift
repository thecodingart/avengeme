//
//  TheatreTableViewCell.swift
//  AvengeMe
//
//  Created by Brandon Levasseur on 11/1/14.
//  Copyright (c) 2014 Brandon Levasseur. All rights reserved.
//

import UIKit

class TheatreTableViewCell: UITableViewCell {
  // MARK: - IBOutlets
  @IBOutlet weak var theatreNameLabel: UILabel!
  
  // MARK: - Properties
  var theatre: MovieTheatre? {
    didSet {
      configureCell()
    }
  }
  
  // MARK: - Utility methods
  private func configureCell() {
    theatreNameLabel.text = theatre?.name
  }

}
