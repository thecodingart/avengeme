//
//  ViewController.swift
//  AvengeMe
//
//  Created by Brandon Levasseur on 11/1/14.
//  Copyright (c) 2014 Brandon Levasseur. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    // MARK: - Properties
    var theatre: MovieTheatre? {
        didSet {
            // Update the view.
            if isViewLoaded() {
                configureView()
                provideDataToChildViewControllers()
            }
        }
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configureView()
        provideDataToChildViewControllers()
        // Prep the navigation item so the back button doesn't disappear
        navigationItem.leftItemsSupplementBackButton = true
        navigationItem.hidesBackButton = false
        
        configureTraitOverrideForSize(view.bounds.size)

    }
    
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        configureTraitOverrideForSize(size)
    }
    
    // MARK: - Utility methods
    private func configureView() {
        
    }
    
    private func provideDataToChildViewControllers() {
        for vc in childViewControllers {
            if let theatreContainer = vc as? TheatreContainer {
                theatreContainer.theatre = theatre
            }
        }
    }
    
    private func configureTraitOverrideForSize(size: CGSize) {
        var traitOverride: UITraitCollection?
        if size.height < 1000 {
            traitOverride = UITraitCollection(verticalSizeClass: .Compact)
        }
        for vc in childViewControllers as [UIViewController] {
            setOverrideTraitCollection(traitOverride, forChildViewController: vc)
        }
    }
    
}

