//
//  MovieTheatre.swift
//  AvengeMe
//
//  Created by Brandon Levasseur on 11/1/14.
//  Copyright (c) 2014 Brandon Levasseur. All rights reserved.
//

import Foundation
import UIKit

@objc class MovieTheatre {
    private(set) var name: String
    private(set) var movieTimes: [NSDate]
    private(set) var image: UIImage
 
    init(name: String, imageName: String, movieTimes: [NSDate]) {
        self.name = name
        self.movieTimes = movieTimes
        image = UIImage(named: imageName)!
    }
}
