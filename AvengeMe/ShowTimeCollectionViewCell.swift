
//
//  ShowTimeCollectionViewCell.swift
//  AvengeMe
//
//  Created by Brandon Levasseur on 11/2/14.
//  Copyright (c) 2014 Brandon Levasseur. All rights reserved.
//

import UIKit

class ShowTimeCollectionViewCell: UICollectionViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var showTimeLabel: UILabel!
    
    // MARK: - Properties
    var showTime: NSDate? {
        didSet {
            configureView()
        }
    }
    
    // MARK: - Utility methods
    private func configureView() {
        if let showTime = showTime {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "hh:mm a"
            showTimeLabel.text = dateFormatter.stringFromDate(showTime)
        }
    }

}
