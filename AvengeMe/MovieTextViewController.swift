//
//  MovieTextViewController.swift
//  AvengeMe
//
//  Created by Brandon Levasseur on 11/2/14.
//  Copyright (c) 2014 Brandon Levasseur. All rights reserved.
//

import UIKit

@objc protocol TheatreContainer {
    var theatre: MovieTheatre? { get set }
}

class MovieTextViewController: UIViewController, TheatreContainer {
    
    // MARK: - IBOutlets
    @IBOutlet weak var theatreNameLabel: UILabel!
    @IBOutlet weak var theatreTimeLabel: UILabel!
    
    // MARK: - Properties
    var theatre: MovieTheatre? {
        didSet {
            if isViewLoaded() {
                configureView()
            }
            provideDataToChildViewControllers()
        }
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        
        let traitOverride = UITraitCollection(horizontalSizeClass: .Compact)
        for vc in childViewControllers as [UIViewController] {
            setOverrideTraitCollection(traitOverride, forChildViewController: vc)
        }
    }
    
    // MARK: - Utility methods
    private func configureView() {
        if let theatre = theatre {
            theatreNameLabel.text = theatre.name
            let showTime = theatre.movieTimes.first! as NSDate
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "hh:mm a"
            let dateString = dateFormatter.stringFromDate(showTime)
            theatreTimeLabel.text = "Next show \n" + dateString
        }
    }
    
    private func provideDataToChildViewControllers() {
        for vc in childViewControllers {
            if let showTimesContainer = vc as? ShowTimesContainer {
                if let showTimes = theatre?.movieTimes {
                    showTimesContainer.showTimes = showTimes
                }
            }
        }
    }

}
