//
//  ShowTimesCollectionViewController.swift
//  AvengeMe
//
//  Created by Brandon Levasseur on 11/2/14.
//  Copyright (c) 2014 Brandon Levasseur. All rights reserved.
//

import UIKit

@objc protocol ShowTimesContainer {
    var showTimes: [NSDate] { get set }
}

class ShowTimesCollectionViewController: UICollectionViewController, ShowTimesContainer {
    var showTimes: [NSDate] = [NSDate]() {
        didSet {
            collectionView.reloadData()
        }
    }
    
    // MARK: UICollectionViewDataSource
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return showTimes.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ShowTimeCollectionViewCell", forIndexPath: indexPath) as ShowTimeCollectionViewCell
        
        // Configure the cell
        cell.showTime = showTimes[indexPath.item]
        
        return cell
    }
}
