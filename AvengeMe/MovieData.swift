//
//  MovieData.swift
//  AvengeMe
//
//  Created by Brandon Levasseur on 11/1/14.
//  Copyright (c) 2014 Brandon Levasseur. All rights reserved.
//

import Foundation

class MovieData {
    private(set) var theatres = [MovieTheatre]()
    
    init(plistName: String) {
        self.theatres = loadMovieData(plistName)
    }
    
    convenience init() {
        self.init(plistName: "MovieData")
    }
    
    private func loadMovieData(plistName: String) -> [MovieTheatre] {
        let plistData = NSDictionary(contentsOfFile: NSBundle.mainBundle().pathForResource(plistName, ofType: "plist")!)
        var theatres = [MovieTheatre]()
        for (theatreName, movieInfo) in plistData as [String: NSDictionary]{
            let movieTimes = movieInfo["showTimes"] as [NSDate]
            let imageName = movieInfo["image"] as String
            theatres.append(MovieTheatre(name: theatreName, imageName:imageName, movieTimes:movieTimes))
        }
        return theatres
    }
    
}