//
//  TraitOverrideViewController.swift
//  AvengeMe
//
//  Created by Brandon Levasseur on 11/3/14.
//  Copyright (c) 2014 Brandon Levasseur. All rights reserved.
//

import UIKit

class TraitOverrideViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        var traitOverride: UITraitCollection? = nil
        if size.width > 320 {
            traitOverride = UITraitCollection(horizontalSizeClass: .Regular)
        }
        
        setOverrideTraitCollection(traitOverride, forChildViewController: childViewControllers[0] as UIViewController)
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
    }

}
