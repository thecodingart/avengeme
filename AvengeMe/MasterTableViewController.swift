//
//  MasterTableViewController.swift
//  AvengeMe
//
//  Created by Brandon Levasseur on 11/1/14.
//  Copyright (c) 2014 Brandon Levasseur. All rights reserved.
//

import UIKit

class MasterTableViewController: UITableViewController {
    var detailViewController: DetailViewController? = nil
    let movieData = MovieData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            detailViewController = controllers[controllers.count-1].topViewController as? DetailViewController
            if let detailViewController = detailViewController {
                detailViewController.theatre = movieData.theatres.first
            }
        }
        self.title = "Theatres"
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
    }
    
    // MARK: - Segues
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow() {
                let controller = (segue.destinationViewController as UINavigationController).topViewController as DetailViewController
                controller.theatre = movieData.theatres[indexPath.row]
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
            }
        }
    }
    
    // MARK: - Table View
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movieData.theatres.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("TheatreTableViewCell", forIndexPath: indexPath) as TheatreTableViewCell
        
        let theatre = movieData.theatres[indexPath.row]
        cell.theatre = theatre
        return cell
    }
    
    // MARK: - Utility Methods
    private func prepareNavigationBarAppearance() {
        let font = UIFont(name: "HelveticaNeue-Light", size: 30)!
        
        let regularVertical = UITraitCollection(verticalSizeClass: .Regular)
        UINavigationBar.appearanceForTraitCollection(regularVertical).titleTextAttributes = [NSFontAttributeName: font]
        
        let compactVertical = UITraitCollection(verticalSizeClass: .Compact)
        UINavigationBar.appearanceForTraitCollection(compactVertical).titleTextAttributes = [NSFontAttributeName:font.fontWithSize(20)]
    }
    


}
